public with sharing class ToolingAPIDemo {
	public static void createAndUpdateClass() {
		// Constructs the Tooling API wrapper (default constructor uses user session Id)		
		ToolingAPI tooling = new ToolingAPI();
		
		// Create class
		ToolingAPI.ApexClass apexClass = new ToolingAPI.ApexClass();
		apexClass.Body = 'public class HelloWorld { }';
		ToolingAPI.SaveResult sr = tooling.createSObject(apexClass);

		// Delete MetadataContainer				
        List<ToolingAPI.MetadataContainer> containers =  
        	(List<ToolingAPI.MetadataContainer>)
                tooling.query(
                    'SELECT Id, Name FROM MetadataContainer WHERE Name = \'UpdateHelloWorld\'').records;
        if ( containers != null && ! containers.isEmpty() )
            tooling.deleteSObject(ToolingAPI.SObjectType.MetadataContainer, containers[0].Id);
				
		// Create MetadataContainer
		ToolingAPI.MetadataContainer container = new ToolingAPI.MetadataContainer();
		container.name = 'UpdateHelloWorld';
        ToolingAPI.SaveResult containerSaveResult = tooling.createSObject(container);
		Id containerId = containerSaveResult.id;

		// Create ApexClassMember and associate them with the MetadataContainer
		ToolingAPI.ApexClassMember apexClassMember = new ToolingAPI.ApexClassMember();		
		apexClassMember.Body = 'public class HelloWorld { public String hello() { return \'Hello\'; } }';
		apexClassMember.ContentEntityId = sr.id;
		apexClassMember.MetadataContainerId = containerId;		
		ToolingAPI.SaveResult apexClassMemberSaveResult = tooling.createSObject(apexClassMember);

		// Create ContainerAysncRequest to deploy the Apex Classes
		ToolingAPI.ContainerAsyncRequest asyncRequest = new ToolingAPI.ContainerAsyncRequest();		
		asyncRequest.metadataContainerId = containerId;
		asyncRequest.IsCheckOnly = false;		
		ToolingAPI.SaveResult asyncRequestSaveResult = tooling.createSObject(asyncRequest);
		
		// The above starts an async background compile, the following needs repeated (polled) to confirm compilation
		ToolingApi.ContainerAsyncRequest containerAsyncRequest = 
			((List<ToolingAPI.ContainerAsyncRequest>)
				tooling.query(
					'SELECT Id, State, MetadataContainerId, CompilerErrors ' + 
					'FROM ContainerAsyncRequest ' + 
					'WHERE Id = \'' + asyncRequestSaveResult.Id + '\'').records)[0];
		System.debug('State is ' + containerAsyncRequest.State);				
	}
	
	public static void dumpFieldIds()  {
		
		// Constructs the Tooling API wrapper (default constructor uses user session Id)		
		ToolingAPI toolingAPI = new ToolingAPI();
		
		// Query CustomObject object by DeveloperName (note no __c suffix required)
		List<ToolingAPI.CustomObject> customObjects = (List<ToolingAPI.CustomObject>) 
			toolingAPI.query('Select Id, DeveloperName, NamespacePrefix From CustomObject Where DeveloperName = \'Test\'').records;
		
		// Query CustomField object by TableEnumOrId (use CustomObject Id not name for Custom Objects)
		ToolingAPI.CustomObject customObject = customObjects[0];
		Id customObjectId = customObject.Id;
		List<ToolingAPI.CustomField> customFields = (List<ToolingAPI.CustomField>) 
			toolingAPI.query('Select Id, DeveloperName, NamespacePrefix, TableEnumOrId From CustomField Where TableEnumOrId = \'' + customObjectId + '\'').records;
		
		// Dump field names (reapply the __c suffix) and their Id's
		System.debug(customObject.DeveloperName + '__c : ' + customObject.Id);
		for(ToolingAPI.CustomField customField : customFields)
			System.debug(
				customObject.DeveloperName + '__c.' + 
				customField.DeveloperName + '__c : ' + 
				customField.Id);
	}
	
	public static void dumpAllFieldNamesWithDate()
	{
	    ToolingAPI toolingAPI = new ToolingAPI();
	    List<ToolingAPI.CustomField> customFields= (List<ToolingAPI.CustomField>)
	       toolingAPI.query('Select FullName, LastModifiedDate From CustomField').records;
	    System.debug(customFields[0].fullName);
	    System.debug(customFields[0].lastModifiedDate);		
	}
	
	public static void dumpAllCustomObjectsWithDate()
	{
	    ToolingAPI toolingAPI = new ToolingAPI();
	    List<ToolingAPI.CustomObject> customObjects= (List<ToolingAPI.CustomObject>)
	       toolingAPI.query('Select DeveloperName, LastModifiedDate From CustomObject').records;
	    System.debug(customObjects[0].DeveloperName);
	    System.debug(customObjects[0].lastModifiedDate);		
	}
	
	public static void createApexClass() {
		
		// Create an Apex class!
		ToolingAPI toolingAPI = new ToolingAPI();
		ToolingAPI.ApexCLass newClass = new ToolingAPI.ApexClass();
		newClass.Name = 'HelloWorld';
		newClass.Body = 'public class HelloWorld { }';
		toolingAPI.createSObject(newClass);		
	}
}